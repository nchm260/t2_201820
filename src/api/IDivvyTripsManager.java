package api;

import java.io.IOException;

import model.data_structures.IDoublyLinkedList;
import model.vo.VOByke;
import model.vo.VOTrip;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IDivvyTripsManager {

	/**
	 * Method to load the Divvy trips of the STS
	 * @param tripsFile - path to the file 
	 * @throws IOException 
	 */
	void loadTrips(String tripsFile) throws IOException;
	
	
	/**
	 * Method to load the Divvy Stations of the STS
	 * @param stationsFile - path to the file 
	 * @throws IOException 
	 */
	void loadServices(String stationsFile) throws IOException;
	

	public IDoublyLinkedList <VOTrip> getTripsOfGender (String gender);
	
	
	public IDoublyLinkedList <VOTrip> getTripsToStation (int stationID);


	
}
