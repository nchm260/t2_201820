package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip 
{

	private int id;
	private	int tripSecs;
	private String fStation;
	private	String tStation;
	private String gender;
	private String tsId;

	public VOTrip (int i, int ts, String fS, String tsid, String tS, String g)
	{
		id = i;
		tripSecs = ts;
		fStation = fS;
		tStation = tS;
		tsId = tsid;
		gender = g;
	}

	/**
	 * @return id - Trip_id
	 */
	public int id() 
	{
		// TODO Auto-generated method stub
		return id;
	}	


	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() 
	{
		// TODO Auto-generated method stub
		return tripSecs;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() 
	{
		// TODO Auto-generated method stub
		return fStation;
	}

	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() 
	{
		// TODO Auto-generated method stub
		return tStation;
	}
	public String getGender()
	{
		return gender;
	}
	public String getToStationId()
	{
		return tsId;
	}
}
