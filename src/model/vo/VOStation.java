package model.vo;

public class VOStation 
{
String id;
String name;
String city;
String latitude;
String longitude;
String dpCapacity;
String date;

public VOStation (String[] values)
{
	id = values[0];
    name = values[1];
    city = values[2];
    latitude = values[3];
    longitude = values[4];
    dpCapacity = values[5];
    date = values[6];
}



}
