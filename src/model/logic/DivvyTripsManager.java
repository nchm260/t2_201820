package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import api.IDivvyTripsManager;
import model.vo.VOByke;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.Node;

public class DivvyTripsManager implements IDivvyTripsManager 
{

	private IDoublyLinkedList <VOTrip> lista;

	public void loadStations (String stationsFile) throws IOException 
	{
		// TODO Auto-generated method stub
		FileReader fr = new FileReader (new File (stationsFile));
		BufferedReader br = new BufferedReader (fr);
		String linea = br.readLine();
		DoublyLinkedList <VOStation> list = new DoublyLinkedList ();
		while (linea != null)
		{
			String[] datos = linea.split(",");
			VOStation elem = new VOStation (datos);
			list.add(elem);
			br.readLine();
		}
		br.close();
		fr.close();

	}


	public void loadTrips (String tripsFile) throws IOException 
	{
		// TODO Auto-generated method stub
		FileReader fr = new FileReader (new File (tripsFile));
		BufferedReader br = new BufferedReader (fr);
		String linea = br.readLine();
		DoublyLinkedList <VOTrip> list = new DoublyLinkedList ();
		while (linea != null)
		{
			String[] datos = linea.split(",");
			VOTrip elem = new VOTrip (Integer.valueOf(datos[0]), Integer.valueOf(datos[4]), datos[6], datos[7], datos[8], datos[10]);
			list.add(elem);
			br.readLine();
		}
		lista = list;
		br.close();
		fr.close();
	}

	@Override
	public IDoublyLinkedList <VOTrip> getTripsOfGender (String gender) 
	{
		// TODO Auto-generated method stub
		IDoublyLinkedList<VOTrip> list = new DoublyLinkedList ();
		Node nodoActual = lista.darPrimero();
		VOTrip actual = (VOTrip) lista.darPrimero().darElem();
		while (actual != null)
		{
			if (actual.getGender().equals(gender))
				list.add(actual);
			nodoActual = nodoActual.darSiguiente();
			actual = (VOTrip) nodoActual.darElem();
		}
		return list;
	}

	@Override
	public IDoublyLinkedList <VOTrip> getTripsToStation (int stationID) 
	{
		// TODO Auto-generated method stub
		IDoublyLinkedList<VOTrip> list = new DoublyLinkedList ();
		Node nodoActual = lista.darPrimero();
		VOTrip actual = (VOTrip) lista.darPrimero().darElem();
		while (actual != null)
		{
			if (Integer.valueOf(actual.getToStationId()) == stationID)
				list.add(actual);
			nodoActual = nodoActual.darSiguiente();
			actual = (VOTrip) nodoActual.darElem();
		}
		return list;

		
	}


	@Override
	public void loadServices(String stationsFile) throws IOException 
	{
		// TODO Auto-generated method stub
		loadStations (stationsFile);
	}


}
