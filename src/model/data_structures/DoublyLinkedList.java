package model.data_structures;

import java.util.Iterator;

public class DoublyLinkedList<T> implements IDoublyLinkedList<T> 
{

	int size;
	Node primero;

	public DoublyLinkedList ()
	{
		size = 0;
		primero = null;
	}
	public Node darPrimero()
	{
		return primero;
	}
	@Override
	public int getSize() 
	{
		// TODO Auto-generated method stub
		return size;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Iterator<T> iterator() 
	{
		// TODO Auto-generated method stub
		return (Iterator<T>) (primero.darActual()!= null ? primero.darActual() : null);
	}

	@Override
	public void add(T e) 
	{
		Node nuevo = new Node (e);
		// TODO Auto-generated method stub
		if (primero != null)
		{
		nuevo.cambiarSiguiente(primero);
		primero.cambiarAnterior(nuevo);
		primero = nuevo;
		}
		else
			primero = nuevo;
	}

	@Override
	public void addAtEnd(T e) 
	{
		// TODO Auto-generated method stub
		Node sig = primero;
		Node nuevo = new Node(e);
		while (sig.darSiguiente() != null)
		{
			sig = (Node) sig.darSiguiente();
		}
		sig.cambiarSiguiente(nuevo);
		nuevo.cambiarAnterior(sig);

	}

	@Override
	public void addAtK(int k, T e) 
	{
		// TODO Auto-generated method stub
		Node sig = primero;
		Node nuevo = new Node(e);
		boolean termino = false;
		int l = 1;
		while (sig.darSiguiente() != null && !termino)
		{
			sig = sig.darSiguiente();
			l++;
			if (l == k)
			{
				termino = true;
			}

		}
		nuevo.cambiarSiguiente(sig.darSiguiente());
		nuevo.cambiarAnterior(sig);
		sig.cambiarSiguiente(nuevo);
		nuevo.darSiguiente().cambiarAnterior(nuevo);
	}

	@Override
	public T getElement(int k) 
	{
		// TODO Auto-generated method stub
		T elem = null;
		if (primero != null)
		{
			Node sig = primero;
			int l = 1;
			while (sig.darSiguiente() != null)
			{
				l++;
				sig = sig.darSiguiente();
				if (l == k)
				{
					elem = (T) sig.darElem();
				}
			}
		}
		return elem;
	}

	@Override
	public T getCurrentElement() 
	{
		// TODO Auto-generated method stub
		return (T) primero.darActual().darElem();
	}

	public void delete(T e) 
	{
		// TODO Auto-generated method stub
		
		
	}
	@Override
	public void deleteAtK(int k) 
	{
		// TODO Auto-generated method stub
	}

}
