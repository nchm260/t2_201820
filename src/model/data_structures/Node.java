package model.data_structures;

public class Node <T> 
{
	private T elem;
	private Node sig;
	private Node ant;
	
	public Node(T e)
	{
		elem = e;
		sig = null;
		ant = null;
		
	}
	public T darElem()
	{
		return elem;
	}
	public Node darSiguiente()
	{
		return sig;
	}
	public Node darAnterior()
	{
		return ant;
	}
	public void cambiarSiguiente(Node nuevo)
	{
		sig = nuevo;
	}
	public void cambiarAnterior(Node nuevo)
	{
		ant = nuevo;
	}
	public Node darActual() 
	{
		// TODO Auto-generated method stub
		return this;
	}
}
