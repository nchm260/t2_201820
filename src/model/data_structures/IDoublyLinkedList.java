package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IDoublyLinkedList<T> extends Iterable<T> 
{
	Node darPrimero();
	int getSize();
	void add(T e);
	void addAtEnd(T e);
	void addAtK(int k, T e);
	T getElement(int k);
	T getCurrentElement();
	void delete(T e);
	void deleteAtK(int k);
	
}
