package controller;

import java.io.IOException;

import api.IDivvyTripsManager;
import model.data_structures.IDoublyLinkedList;
import model.logic.DivvyTripsManager;
import model.vo.VOByke;
import model.vo.VOTrip;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	private static IDivvyTripsManager  manager = new DivvyTripsManager();

	public static void loadStations() 
	{
		try {
			manager.loadServices("./data/Divvy_Stations_2017_Q3Q4.csv");
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	}

	public static void loadTrips() 
	{
		try 
		{
			manager.loadTrips("./data/Divvy_Trips_2017_Q3.csv");
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());	
		}
	}

	public static IDoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		return null;
	}

	public static IDoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		return null;
	}
}
